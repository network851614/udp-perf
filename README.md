# udp-perf

## Purpose
udp-perf is another tool to test the performance of Ethernet network devices.
The goal is to measure
- packet loss
- out of order packets
- packet delay and packet delay variation
this can be done with different set of traffic patterns specified by length of packet and burstiness.

## Setup
There are 3 possible setup's for the DUT (Device Under Test)

	+----------+     +-------------+     +----------+
	|          |     |             |     |          |
	|  Client  |-----|     DUT     |-----|  Server  |
	|          |     |             |     |          |
	+----------+     +-------------+     +----------+

Client and server run on separate device, DUT is transmitting the packets (e.g. Switch, Bridge, Transmission Equipment,..) 

	+----------+     +---------------+
	|          |     |               |
	|  Client  |-----|  DUT  Server  |
	|          |     |               |
	+----------+     +---------------+

	+----------+     +---------------+
	|          |     |               |
	|  Server  |-----|  DUT  Client  |
	|          |     |               |
	+----------+     +---------------+

The device that runs the client or server itself is the DUT (e.g. Embedded Linux System,...)



# Install tools
## Install Rust compiler
see https://www.rust-lang.org/tools/install


## Install GCC
For debian you can install it with apt depending on the required architectures
```
sudo apt install gcc
sudo apt install gcc-arm-linux-gnueabihf 
sudo apt install gcc-aarch64-linux-gnu
```

# Build target
## amd64
```
cargo build --release
```

## arm-linux-gnueabihf 
```
cargo build --release --target armv7-unknown-linux-gnueabihf
scp target/armv7-unknown-linux-gnueabihf/release/udp-perf root@<target>:/usr/bin
```

## aarch64-linux-gnu 
```
cargo build --release --target aarch64-unknown-linux-gnu
scp target/aarch64-unknown-linux-gnu/release/udp-perf root@<target>:/usr/bin
```


# Run
use `udp-perf --help` for details

```
Usage: udp-perf <COMMAND>

Commands:
  server  start server (no parameters required)
  client  start client (parameter see  client --help)
  version print version of tool
  help    print this message or the help of the given subcommand(s)

Options:
  -h, --help  print help
```

```
Usage: udp-perf client [OPTIONS] --ip <IP> --packets <PACKETS>

Options:
  -i, --ip <IP>                  sent to IP address IPv4 169.254.1.100 or IPv6 fe80::219:5ff:fe10:400a
  -p, --packets <PACKETS>        sent nbr of packets (1 to 1'000'000)
  -r, --runs <RUNS>              number of runs (1 to 1'000'000) [default: 1]
  -m, --mode <MODE>              pattern mode ('std', 'reduced', 'max-pdu-size' or name of pattern in config file) [default: std]
  -c, --config <FILE>            pattern config file
  -t, --rx-timeout <RX_TIMEOUT>  rx timeout in ms (50 to 10'000) [default: 100]
  -h, --help                     print help
```

## Mode parameters
The mode parameter specifies the traffic pattern 

### std
```
1 packet (back to back) with length 200, 400, 800, 1400, 1520, 5000 [Bytes]
2 packet (back to back) with length 200, 400, 800, 1400, 1520, 5000 [Bytes]
8 packet (back to back) with length 200, 400, 800, 1400, 1520, 4500 [Bytes]
```

### reduced
```
1 packet (back to back) with length 200, 400, 800, 1400, 1512, 1514 [Bytes]
2 packet (back to back) with length 200, 400, 800, 1400, 1512, 1514 [Bytes]
3 packet (back to back) with length 200, 400, 800, 1000             [Bytes]
```

### max-pdu-size
```
1 packet (back to back) with length 1512, 1514, 1516 1518, 1520, 1522 [Bytes]
```

### name with pattern from config file
you can define your own packet patterns in the config file in yaml    
See `udp-perf.conf.sample`

```
client --ip <ip-addr> --packets 100 --mode low --config udp-perf.conf.sample
```

## Run server
```
udp-perf server
```

## Run client
```
udp-perf client --ip <ip-addr> --packets 100 --mode std
```

## Results
```
./target/release/udp-perf client --ip 169.254.3.127 --packets 1000

------ udp performance client run 1 of 1 ------
  len*    pkt*  times,mean_ms,min_ms,max_ms, l_err, r_err,  Mbps,   Pps,  <0.6,<1,<1.6,<2.6,<4,<6.5,<10,<16,<26,<40,<65,<100,<160,<260,<400,<650,<1000,<[ms]
  200*   1Pkt*   1000,    0.3,   0.3,   2.7,     0,     0,   4.3,  2732,  991,2,4,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,
  400*   1Pkt*   1000,    0.4,   0.4,   1.8,     0,     0,   6.7,  2114,  988,4,5,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  800*   1Pkt*   1000,    0.6,   0.6,   2.0,     0,     0,   9.2,  1449,  0,985,10,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 1000*   1Pkt*   1000,    0.7,   0.7,   1.2,     0,     0,  10.1,  1265,  0,994,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 1400*   1Pkt*   1000,    1.0,   0.9,   2.3,     0,     0,  11.0,   986,  0,655,339,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 1520*   1Pkt*   1000,    1.1,   1.0,   2.5,     0,     0,  10.8,   889,  0,0,984,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 5000*   1Pkt*   1000,    1.7,   1.6,   3.2,     0,     0,  22.6,   565,  0,0,0,995,5,0,0,0,0,0,0,0,0,0,0,0,0,0,
  200*   2Pkt*    500,    0.4,   0.3,   2.0,     0,     0,   7.3,  4566,  488,5,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  400*   2Pkt*    500,    0.5,   0.5,   2.0,     0,     0,  11.5,  3623,  489,4,1,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  800*   2Pkt*    500,    0.8,   0.6,   2.2,     0,     0,  15.8,  2475,  0,488,8,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 1000*   2Pkt*    500,    0.9,   0.7,   2.4,     0,     0,  17.2,  2150,  0,470,19,11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 1400*   2Pkt*    500,    1.1,   1.1,   2.5,     0,     0,  19.1,  1709,  0,0,492,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 1520*   2Pkt*    500,    1.3,   1.2,   2.4,     0,     0,  18.0,  1483,  0,0,486,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 5000*   2Pkt*    500,    2.2,   2.1,   3.6,     0,     0,  35.6,   892,  0,0,0,480,20,0,0,0,0,0,0,0,0,0,0,0,0,0,
  200*   8Pkt*    125,    0.9,   0.8,   1.2,     0,     0,  13.5,  8474,  0,111,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  400*   8Pkt*    125,    0.9,   0.8,   2.3,     0,     0,  27.1,  8474,  0,105,17,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  800*   8Pkt*    125,    1.3,   1.2,   2.7,     0,     0,  38.3,  5988,  0,0,116,7,2,0,0,0,0,0,0,0,0,0,0,0,0,0,
 1000*   8Pkt*    125,    1.5,   1.4,   2.9,     0,     0,  40.6,  5076,  0,0,113,7,5,0,0,0,0,0,0,0,0,0,0,0,0,0,
 1400*   8Pkt*    125,    2.0,   1.9,   3.3,     0,     0,  43.7,  3906,  0,0,0,121,4,0,0,0,0,0,0,0,0,0,0,0,0,0,
 1520*   8Pkt*    125,    2.3,   2.1,   3.6,     0,     0,  41.6,  3424,  0,0,0,111,14,0,0,0,0,0,0,0,0,0,0,0,0,0,
 4500*   8Pkt*    125,    4.6,   4.4,   5.7,     0,     0,  61.9,  1721,  0,0,0,0,0,125,0,0,0,0,0,0,0,0,0,0,0,0,

****** udp client finished sucessfully ******
```
```
len:     length of packet
pkt:     back to back packet count
times:   count of bursts
mean_ms: mean transmission time in ms
min_ms:  minimum transmission time in ms
max_ms:  maximum transmission time in ms
l_err:   local detected errors (at client side, timeout, seqence-errors, payload-errors,...)
r_err:   remote detected errors (at server side)
Mbps:    throughput MBit/s
PPs:     packet/s
hist:    comma separated histogram of packet transmission time in ms 
         <0.6,<1,<1.6,<2.6,<4,<6.5,<10,<16,<26,<40,<65,<100,<160,<260,<400,<650,<1000,<[ms] 
```

## Config File
In the config file you can specify your own packet patterns in yaml format.

Description of your packet sequence patterns collection
```
description = 'Sample collection of packet sequence patterns'
```

Pattern containing
- name (used as mode parameter)
- description
- sequence of lenght, burst size pair

```
[pattern.low]
name = 'low'
description = 'pattern for low resource equipement'
sequence = [[80, 1],
            [128, 1],
            [256, 1],]
```

# Future Plans
- remove all FIXME
- improve error handling / loging
- add unit test



