// clippy doc: https://doc.rust-lang.org/stable/clippy/index.html?highlight=clippy%3A%3Aall#clippy
#![warn(clippy::pedantic)]
#![warn(clippy::cargo)]
#![warn(clippy::module_name_repetitions)]
#![warn(clippy::missing_errors_doc)]
#![warn(clippy::cargo_common_metadata)]
#![warn(clippy::enum_variant_names)]
#![warn(clippy::needless_pass_by_value)]

use clap::{Parser, Subcommand};
use clap_num::number_range;
use client::Client;
use server::Server;
use std::{process::ExitCode, time::Duration};

mod client;
mod packet;
mod server;
mod settings;

fn range_packet_nbr(s: &str) -> Result<u32, String> {
    number_range(s, 1, 1_000_000)
}

fn range_runs(s: &str) -> Result<u32, String> {
    number_range(s, 1, 1_000_000)
}

fn range_rx_timeout(s: &str) -> Result<Duration, String> {
    Ok(std::time::Duration::from_millis(number_range(
        s, 50, 10_000,
    )?))
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// start server (no parameters required)
    Server {},
    /// start client (parameter see  client --help)
    Client {
        /// sent to IP address IPv4 169.254.1.100 or IPv6 `fe80::219:5ff:fe10:400a`
        #[arg(short, long)]
        ip: std::net::IpAddr,

        /// sent nbr of packets (1 to 1'000'000)
        #[clap(short, long, value_parser=range_packet_nbr)]
        packets: u32,

        /// number of runs (1 to 1'000'000)
        #[clap(short, long, value_parser=range_runs, default_value_t=1)]
        runs: u32,

        /// pattern mode ('std', 'reduced', 'max-pdu-size' or name of pattern in config file)
        #[clap(short, long, value_parser, default_value_t=String::from("std"))]
        mode: String,

        /// pattern config file
        #[clap(short, long, value_parser, value_name = "FILE")]
        config: Option<std::path::PathBuf>,

        /// rx timeout in ms (50 to 10'000)
        #[clap(short = 't', long, value_parser=range_rx_timeout, default_value="100")]
        rx_timeout: std::time::Duration,
    },
    /// print version of tool
    Version {},
}

#[derive(Parser, Debug)]
struct Args {
    #[command(subcommand)]
    command: Commands,
}

fn main() -> ExitCode {
    let args = Args::parse();
    let port = 4888;
    let playload_type: packet::PayloadType = packet::PayloadType::AlterernatingIncrement;

    match &args.command {
        Commands::Server {} => match Server::try_run(port, playload_type) {
            Ok(()) => ExitCode::SUCCESS,
            Err(_) => ExitCode::from(1),
        },
        Commands::Client {
            ip,
            packets,
            runs,
            mode,
            config,
            rx_timeout,
        } => {
            match Client::try_run_perf(
                mode.clone(),
                *ip,
                port,
                *packets,
                *runs,
                playload_type,
                config,
                *rx_timeout,
            ) {
                Ok(has_errors) => {
                    if has_errors {
                        ExitCode::from(3)
                    } else {
                        ExitCode::SUCCESS
                    }
                }
                Err(_) => ExitCode::from(2),
            }
        }
        Commands::Version {} => {
            let name = env!("CARGO_PKG_NAME");
            let version = env!("CARGO_PKG_VERSION");
            println!("{name} {version}");
            ExitCode::SUCCESS
        }
    }
}
