use crate::packet;
use std::collections::HashMap;
use std::net::IpAddr;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::net::SocketAddrV4;
use std::net::SocketAddrV6;
use std::net::UdpSocket;

#[derive(thiserror::Error, Debug)]
/// Errors
pub enum Error {
    #[error("Cannot bind socket")]
    CannotBindSocket(std::io::Error),
}

#[derive(Debug)]
pub struct Stats {
    pub rx_total: u32,  /* rx packets */
    pub tx_error: u32,  /* tx errors */
    pub seq_error: u32, /* tx errors */
    pub len_err: u32,   /* len errors */
    pub pld_error: u32, /* bit errors in payload */
}

impl Stats {
    pub fn new() -> Self {
        Self {
            rx_total: 0,
            tx_error: 0,
            seq_error: 0,
            len_err: 0,
            pld_error: 0,
        }
    }
    pub fn clean(&mut self) {
        self.rx_total = 0;
        self.tx_error = 0;
        self.seq_error = 0;
        self.len_err = 0;
        self.pld_error = 0;
    }
}

#[derive(Debug)]
pub struct ClientData {
    pub exp_seq: u32, /* next expected seq */
    pub stats: Stats, /* statistics */
}

impl ClientData {
    pub fn new() -> Self {
        Self {
            exp_seq: 0,
            stats: Stats::new(),
        }
    }
}

#[derive(Debug)]
pub struct Server {
    socket: UdpSocket,
    // FIXME allow more than 1 Client per src_ip, carefull about performance and nbr of hash entries!
    // clients: HashMap<std::net::SocketAddr, ClientData>,
    clients: HashMap<std::net::IpAddr, ClientData>,
    global_rx_error: u32,
}

impl Server {
    pub fn try_new(port: u16) -> Result<Self, Error> {
        // try to bind with IPv6 works for IPv6 and IPv4 on dual stack system
        // if fails try to bind with IPv4 only
        if let Ok(socket) = UdpSocket::bind(SocketAddrV6::new(Ipv6Addr::UNSPECIFIED, port, 0 ,0)) {
            Ok(Self {
                socket,
                clients: HashMap::with_capacity(20),
                global_rx_error: 0,
            })
        } else {
            println!("Server: cannot use dualstack IPv4/IPv6, fallback to IPv4 only");
            let socket: UdpSocket = UdpSocket::bind(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, port))
            .map_err(Error::CannotBindSocket)?;
            Ok(Self {
                socket,
                clients: HashMap::with_capacity(20),
                global_rx_error: 0,
            })
        }
    }

    pub fn handle_packets(&mut self, playload_type: packet::PayloadType) {
        let mut buf = [0; packet::MAX_PACKET_SIZE];
        // recive packet
        match self.socket.recv_from(&mut buf) {
            Ok((size, socket_addr)) => {
                let packet = &buf[..size];
                match packet::check(packet, playload_type) {
                    Ok(r) => {
                        let source = socket_addr.ip();
                        if let std::collections::hash_map::Entry::Vacant(e) = self.clients.entry(source) {
                            e.insert(ClientData::new());
                            println!("Server: register addr {source}");
                        }
                        let client_data = self.clients.get_mut(&source).unwrap(); // FIXME we added it right now
                        // println!("client={client_data:?}");

                        let header_len: usize;
                        match source {
                            IpAddr::V4(_ipv4) => { header_len = packet::HEADER_LEN_IPV4; }
                            IpAddr::V6(ipv6) => {
                                match ipv6.to_ipv4_mapped() {
                                    Some(_) => { header_len = packet::HEADER_LEN_IPV4; }
                                    None => { header_len = packet::HEADER_LEN_IPV6; }
                                };
                            }
                        }

                        // packet match size
                        if (size + header_len) == r.header.exp_packet_len as usize {
                            
                            // first packet of sequence
                            if r.header.seq_nbr == 1 {
                                client_data.exp_seq = 1;
                                client_data.stats.clean();
                                println!("Server: Rx-Msg {:10} of {:10} from {} size {:5}(test started)", 
                                        client_data.stats.rx_total, r.header.seq_len, source, r.header.exp_packet_len);
                            }
                            // sequence match
                            if client_data.exp_seq == r.header.seq_nbr {
                                client_data.exp_seq += 1;
                                client_data.stats.rx_total += 1;
                                client_data.stats.pld_error += r.payload_errors;
                                let mut packet = &buf[..size];
                                let ext_packet: Vec<u8>;


                                // every 10_000 packet print
                                if (r.header.seq_nbr % 10_000) == 0 {
                                    println!("Server: Rx-Msg {:10} of {:10} from {} size {:5}(client-err: seq={} tx={} len={}) (glob-err: rx={})",
                                            client_data.stats.rx_total, r.header.seq_len, 
                                            source, r.header.exp_packet_len, 
                                            client_data.stats.seq_error, client_data.stats.tx_error,
                                            client_data.stats.len_err, self.global_rx_error);
                                }
                                if r.header.seq_nbr == r.header.seq_len {
                                    println!("Server: Rx-Msg {:10} of {:10} from {} size {:5}(client-err: seq={} tx={} len={}) (glob-err: rx={}) bit error={})(seq end)",
                                        client_data.stats.rx_total, r.header.seq_len, 
                                        source, r.header.exp_packet_len, 
                                        client_data.stats.seq_error, client_data.stats.tx_error,
                                        client_data.stats.len_err, self.global_rx_error,
                                        client_data.stats.pld_error);

                                        // FIXME remove extened packet, integrate it in standart
                                        ext_packet = packet::prepare_extended(r.header.seq_nbr,
                                            r.header.seq_len,
                                            size + header_len,
                                            header_len,
                                            client_data.stats.tx_error +
                                            client_data.stats.seq_error +
                                            client_data.stats.len_err +
                                            client_data.stats.pld_error +
                                            self.global_rx_error,
                                            playload_type);
                                            packet = &ext_packet[..size];
                   
                                          client_data.exp_seq = 1;
                                          client_data.stats.clean();
                                }
                                if let Err(e) = self.socket.send_to(packet, socket_addr) {
                                    client_data.stats.tx_error += 1;
                                    println!("Server: Failed to send {e}");
                                }
                            } else {
                                client_data.stats.seq_error += 1;
                                println!("Server: Wrong seq nbr {:10} instead of {:10} from {} size {:5} (adjust seq) (client-err: seq={} tx={} len={})(glob-err: rx={})", 
                                        r.header.seq_nbr, client_data.exp_seq, 
                                        source, r.header.exp_packet_len,
                                        client_data.stats.seq_error, client_data.stats.tx_error,
                                        client_data.stats.len_err, self.global_rx_error);
                                client_data.exp_seq = r.header.seq_nbr + 1;
                            }
                            // println!("Ok {:?}", r);
                            
                        } else {
                            println!("Server: Invalid message size received - Expected {} Arrived {}", 
                            r.header.exp_packet_len, size + header_len);
                            client_data.stats.len_err += 1;
                        }
                    }
                    Err(e) => {
                        //self.stats.packet_stats.rx_error += 1;
                        println!("Err: {e:?}");
                    }
                }
                //println!("ok: size={size} add={:?}", socket_addr);
            }
            Err(e) => {
                self.global_rx_error += 1;
                println!("Err: {e:?}");
            }
        }
    }

    pub fn try_run(port: u16, playload_type: packet::PayloadType) -> Result<(), Error> {
        let mut server = Server::try_new(port)?;
        println!("------ udp-perf server started ------");
        loop {
            server.handle_packets(playload_type);
        }
    }
}
