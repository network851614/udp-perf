use scanf::sscanf;
use std::str;

pub const HEADER_LEN_IPV4: usize = 14 + 20 + 8; // MAC + IPv6 + UDP
pub const HEADER_LEN_IPV6: usize = 14 + 40 + 8; // MAC + IPv6 + UDP
pub const MAX_PACKET_SIZE: usize = 5000;

#[derive(thiserror::Error, Debug)]
#[allow(dead_code)]
/// Errors
pub enum Error {
    #[error("Cannot find header delimiter")]
    CannotFindHeaderDelimiter,
    #[error("Cannot convert to UTF-8")]
    ConvertUtf8(std::str::Utf8Error),
    #[error("Cannot parse packet header")]
    CannotParsePacketHeader(),
}

#[derive(Debug, Clone, Copy)]
#[allow(dead_code)]
/// Payload Type
pub enum PayloadType {
    Unspecified = 0,
    AlterernatingIncrement = 1,
}

#[allow(clippy::cast_possible_truncation)]
fn as_u8(x: usize) -> u8 {
    x as u8 // allow wrap
}

pub fn prepare_header(
    seq_nbr: u32,
    seq_len: u32,
    packet_len: usize,
    playload_type: PayloadType,
) -> String {
    format!(
        "UPi q={} o={} z={} t={}\n\0",
        seq_nbr, seq_len, packet_len, playload_type as u32
    )
}

pub fn prepare_extended_header(
    seq_nbr: u32,
    seq_len: u32,
    packet_len: usize,
    remote_error: u32,
    playload_type: PayloadType,
) -> String {
    format!(
        "UPi q={} o={} z={} t={} e={}\n\0",
        seq_nbr, seq_len, packet_len, playload_type as u32, remote_error
    )
}

pub fn prepare_extended(
    seq_nbr: u32,
    seq_len: u32,
    packet_len: usize,
    header_len: usize,
    remote_error: u32,
    playload_type: PayloadType,
) -> Vec<u8> {
    let mut packet = Vec::new();
    packet.extend_from_slice(
        prepare_extended_header(seq_nbr, seq_len, packet_len, remote_error, playload_type)
            .as_bytes(),
    );
    let app_header_len = packet.len();
    let playload_len = packet_len - header_len - app_header_len;
    let mut payload = vec![0; playload_len];
    // TODO use iterator, make it generic for other payload types
    for (i, element) in payload.iter_mut().enumerate() {
        if (i + app_header_len) % 2 == 1 {
            *element = as_u8((i + app_header_len) / 2);
        } else {
            *element = !as_u8((i + app_header_len) / 2);
        }
        //println!("pattern {} {} ", i+app_header_len, payload[i]);
    }
    packet.extend_from_slice(&payload);
    // println!("len={} {:?}", packet.len(), packet);
    packet
}

pub fn prepare(
    seq_nbr: u32,
    seq_len: u32,
    packet_len: usize,
    header_len: usize,
    playload_type: PayloadType,
) -> Vec<u8> {
    let mut packet = Vec::new();
    packet
        .extend_from_slice(prepare_header(seq_nbr, seq_len, packet_len, playload_type).as_bytes());
    let app_header_len = packet.len();
    let playload_len = packet_len - header_len - app_header_len;
    let mut payload = vec![0; playload_len];
    // TODO use iterator, make it generic for other payload types
    for (i, element) in payload.iter_mut().enumerate() {
        if (i + app_header_len) % 2 == 1 {
            *element = as_u8((i + app_header_len) / 2);
        } else {
            *element = !as_u8((i + app_header_len) / 2);
        }
        //println!("pattern {} {} ", i+app_header_len, payload[i]);
    }
    packet.extend_from_slice(&payload);
    // println!("len={} {:?}", packet.len(), packet);
    packet
}

pub fn find_null_delimiter(buffer: &[u8]) -> Result<usize, Error> {
    // TODO remove '\n'
    if let Some(i) = buffer.iter().position(|w| w == &0) {
        if buffer[i - 1] == b'\n' {
            Ok(i - 1)
        } else {
            Ok(i)
        }
    } else {
        Err(Error::CannotFindHeaderDelimiter)
    }
}

#[derive(Debug)]
#[allow(unused)]
pub struct RxHeaderResults {
    pub seq_nbr: u32,
    pub seq_len: u32,
    pub exp_packet_len: u32,
    pub payload_type: u32,
    pub remote_errors: u32,
}

#[derive(Debug)]
pub struct RxPacketResults {
    pub header: RxHeaderResults,
    pub payload_errors: u32,
}

pub fn decode_header(buf: &[u8]) -> Result<(RxHeaderResults, usize), Error> {
    // println!("buf      ={:?}", buf);
    let i = find_null_delimiter(buf)?;
    let buf_slice = &buf[0..i];
    // println!("buf_slice={:?}", buf_slice);
    let header_str = str::from_utf8(buf_slice).map_err(Error::ConvertUtf8)?;

    // println!("str={header_str}=");
    // TODO always same return header
    let mut seq_nbr = 0;
    let mut seq_len = 0;
    let mut exp_packet_len = 0;
    let mut payload_type = 0;
    let mut remote_errors = 0;

    if sscanf!(
        header_str,
        "UPi q={u32} o={u32} z={u32} t={u32} e={u32}",
        seq_nbr,
        seq_len,
        exp_packet_len,
        payload_type,
        remote_errors
    )
    .is_ok()
    {
        if false {
            println!(
                "seq={seq_nbr} oof={seq_len} size={exp_packet_len} type={payload_type} rerr={remote_errors}"
            );
        }
        Ok((
            RxHeaderResults {
                seq_nbr,
                seq_len,
                exp_packet_len,
                payload_type,
                remote_errors,
            },
            i,
        ))
    } else if sscanf!(
        header_str,
        "UPi q={u32} o={u32} z={u32} t={u32}",
        seq_nbr,
        seq_len,
        exp_packet_len,
        payload_type
    )
    .is_ok()
    {
        if false {
            println!("seq={seq_nbr} oof={seq_len} size={exp_packet_len} type={payload_type}",);
        }
        Ok((
            RxHeaderResults {
                seq_nbr,
                seq_len,
                exp_packet_len,
                payload_type,
                remote_errors: 0,
            },
            i,
        ))
    } else {
        Err(Error::CannotParsePacketHeader())
    }
}

pub fn check(buf: &[u8], playload_type: PayloadType) -> Result<RxPacketResults, Error> {
    let result = decode_header(buf)?;

    match playload_type {
        PayloadType::Unspecified => {
            // don't check payload
            Ok(RxPacketResults {
                header: result.0,
                payload_errors: 0,
            })
        }
        PayloadType::AlterernatingIncrement => {
            let app_header_len = result.1 + 2; // FIXME +1 if newline removed
            let mut payload_errors = 0;
            let payload = &buf[app_header_len..];
            // println!("app_header_len={app_header_len} rx_size={rx_size}");
            // println!("pl={:?}", payload);
            // TODO use iterator

            for (i, element) in payload.iter().enumerate().take(buf.len() - app_header_len) {
                let expected_payload: u8 = if (i + app_header_len) % 2 == 1 {
                    as_u8((i + app_header_len) / 2)
                } else {
                    !as_u8((i + app_header_len) / 2)
                };
                if *element != expected_payload {
                    payload_errors += 1;
                }
            }
            Ok(RxPacketResults {
                header: result.0,
                payload_errors,
            })
        }
    }
}
