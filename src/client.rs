use crate::packet;
use crate::settings;
use settings::Settings;
use std::fmt::Write;
use std::net::IpAddr;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::net::SocketAddr;
use std::net::SocketAddrV4;
use std::net::SocketAddrV6;
use std::net::UdpSocket;
use std::thread;
use std::time::Duration;
use std::time::Instant;

#[derive(thiserror::Error, Debug)]
/// Errors
pub enum Error {
    #[error("Cannot bind socket")]
    CannotBindSocket(std::io::Error),
    #[error("Cannot set recive timeout")]
    CannotSetReciveTimeout(std::io::Error),
    #[error("Cannot send packet")]
    CannotSendPacket,
    #[error("Recive timeout")]
    RecieveTimeout,
    #[error("Missing config file, --config /<path-to-configfile>/<configfile.conf>")]
    MissingConfigFile(),
    #[error("Cannot read config file: {1} {0}")]
    CannotReadConfigFile(std::io::Error, std::path::PathBuf),
    #[error("#[from] Cannot parse config file: {0}")]
    CannotParseConfigFile(toml::de::Error),
    #[error("#[from] Cannot decode the config file {0}")]
    CannotDecodeConfigFile(std::str::Utf8Error),
    #[error("Cannot find pattern '{0}' in config file")]
    CannotFindPattern(String),
}

#[derive(Debug)]
struct PacketStats {
    pub tx_packets: u32,
    pub rx_packets: u32,
    pub tx_error: u32,
    pub rx_error: u32,
    pub rx_timeout: u32,
    pub rx_seq_error: u32,
    pub rx_len_error: u32,
    pub rx_pld_error: u32,
    pub rx_remote_error: u32,
}

impl PacketStats {
    fn new() -> Self {
        Self {
            tx_packets: 0,
            rx_packets: 0,
            tx_error: 0,
            rx_error: 0,
            rx_timeout: 0,
            rx_seq_error: 0,
            rx_len_error: 0,
            rx_pld_error: 0,
            rx_remote_error: 0,
        }
    }
    pub fn clear(&mut self) {
        self.tx_packets = 0;
        self.rx_packets = 0;
        self.tx_error = 0;
        self.rx_error = 0;
        self.rx_timeout = 0;
        self.rx_seq_error = 0;
        self.rx_len_error = 0;
        self.rx_pld_error = 0;
        self.rx_remote_error = 0;
    }
}

pub const DELAYSTATS_TIMESLOTS: &[u32] = &[
    /*       [us] */
    650, /* 0.65ms */
    1_000, 1_600, 2_600, 4_000, 6_500, /* 1-10ms */
    10_000, 16_000, 26_000, 40_000, 65_000, /* 10-100ms */
    100_000, 160_000, 260_000, 400_000, 650_000,   /* 100-1000ms */
    1_000_000, /* >1000ms */
];

#[allow(clippy::cast_possible_truncation)]
fn as_u32(x: u128) -> u32 {
    x as u32 // allow wrap
}

#[derive(Debug)]
pub struct DelayStats {
    pub min_delay_us: u32,
    pub max_delay_us: u32,
    pub sum_delay_us: u32,
    pub count_measurements: u32,
    pub time_hist_us: [u32; DELAYSTATS_TIMESLOTS.len() + 1],
}

impl DelayStats {
    fn new() -> Self {
        Self {
            min_delay_us: 10_000_000,
            max_delay_us: 0,
            sum_delay_us: 0,
            count_measurements: 0,
            time_hist_us: [0; DELAYSTATS_TIMESLOTS.len() + 1],
        }
    }

    pub fn clear(&mut self) {
        self.min_delay_us = 10_000_000;
        self.max_delay_us = 0;
        self.sum_delay_us = 0;
        self.count_measurements = 0;
        self.time_hist_us = [0; DELAYSTATS_TIMESLOTS.len() + 1];
    }

    pub fn add_measurement(&mut self, time_us: u32) {
        if time_us < self.min_delay_us {
            self.min_delay_us = time_us;
        };
        if time_us > self.max_delay_us {
            self.max_delay_us = time_us;
        };
        self.sum_delay_us += time_us;
        self.count_measurements += 1;
        match DELAYSTATS_TIMESLOTS.iter().position(|&x| time_us <= x) {
            Some(pos) => {
                self.time_hist_us[pos] += 1;
            }
            None => {
                self.time_hist_us[self.time_hist_us.len() - 1] += 1;
            }
        }
    }
}

#[derive(Debug)]
struct Stats {
    pub packet_stats: PacketStats,
    pub delay_stats: DelayStats,
}

impl Stats {
    pub fn new() -> Self {
        Self {
            packet_stats: PacketStats::new(),
            delay_stats: DelayStats::new(),
        }
    }
    pub fn clear(&mut self) {
        self.packet_stats.clear();
        self.delay_stats.clear();
    }
}

#[derive(Debug)]
pub struct Client {
    dst_ip: IpAddr,
    dst_port: u16,
    stats: Stats,
    socket: UdpSocket,
    header_len: usize,
}

impl Client {
    fn try_new(
        dst_ip: IpAddr,
        dst_port: u16,
        rx_timeout: std::time::Duration,
    ) -> Result<Self, Error> {
        let socket_addr;
        let header_len;
        if dst_ip.is_ipv6() {
            socket_addr = SocketAddr::V6(SocketAddrV6::new(Ipv6Addr::UNSPECIFIED, 0, 0, 0));
            header_len = packet::HEADER_LEN_IPV6;
        } else {
            socket_addr = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, 0));
            header_len = packet::HEADER_LEN_IPV4;
        }

        let socket: UdpSocket = UdpSocket::bind(socket_addr).map_err(Error::CannotBindSocket)?;
        socket
            .set_read_timeout(Some(rx_timeout))
            .map_err(Error::CannotSetReciveTimeout)?;

        Ok(Self {
            dst_ip,
            dst_port,
            stats: Stats::new(),
            socket,
            header_len,
        })
    }

    fn send(
        &mut self,
        seq: u32,
        seq_len: u32,
        packet_len: usize,
        playload_type: packet::PayloadType,
    ) -> Result<(), Error> {
        let buf = packet::prepare(seq, seq_len, packet_len, self.header_len, playload_type);
        //println!("ip {:?}  port {:?}  {:#?}", self.dst_ip, self.dst_port, SocketAddr::new(self.dst_ip, self.dst_port));
        if let Err(e) = self
            .socket
            .send_to(&buf, SocketAddr::new(self.dst_ip, self.dst_port))
        {
            self.stats.packet_stats.tx_error += 1;
            println!("ERROR: cannot send packet {e}");
            Err(Error::CannotSendPacket)
        } else {
            self.stats.packet_stats.tx_packets += 1;
            Ok(())
        }
    }
    fn recive(
        &mut self,
        exp_seq: u32,
        exp_seq_len: u32,
        exp_packet_len: usize,
        playload_type: packet::PayloadType,
    ) -> Result<(), Error> {
        let mut buf = [0; 10000];
        match self.socket.recv_from(&mut buf) {
            Ok((size, _socket_addr)) => {
                self.stats.packet_stats.rx_packets += 1;
                if exp_packet_len != (size + self.header_len) {
                    self.stats.packet_stats.rx_len_error += 1;
                }
                // println!("recv {} Bytes", size);
                // println!("recv {:?}", buf);
                let packet = &buf[..size];
                match packet::check(packet, playload_type) {
                    Ok(r) => {
                        if exp_seq != r.header.seq_nbr {
                            self.stats.packet_stats.rx_seq_error += 1;
                            // println!("ERR: exp_seq {} != {}  rx_seq_error {}", exp_seq, r.header.seq_nbr, self.stats.packet_stats.rx_seq_error);
                        }
                        if exp_packet_len != r.header.exp_packet_len as usize {
                            self.stats.packet_stats.rx_len_error += 1;
                        }
                        if exp_seq_len != r.header.seq_len {
                            self.stats.packet_stats.rx_error += 1;
                        }
                        self.stats.packet_stats.rx_remote_error += r.header.remote_errors;
                        self.stats.packet_stats.rx_pld_error += r.payload_errors;
                        // println!("Ok {:?}", r);
                    }
                    Err(e) => {
                        self.stats.packet_stats.rx_error += 1;
                        println!("Err: {e:?}");
                    }
                }
                Ok(())
            }
            Err(_e) => {
                self.stats.packet_stats.rx_timeout += 1;
                // println!("Err: {:?}", Error::RecieveTimeout);
                Err(Error::RecieveTimeout)
            }
        }
    }

    fn print_header() {
        println!("  len*    pkt*  times,mean_ms,min_ms,max_ms, l_err, r_err,  Mbps,   Pps,  <0.6,<1,<1.6,<2.6,<4,<6.5,<10,<16,<26,<40,<65,<100,<160,<260,<400,<650,<1000,<[ms]");
    }

    fn print_statistics(&self, packet_len: usize, nbr_packets: u32, burst_size: u32) {
        let p = &self.stats.packet_stats;
        let d = &self.stats.delay_stats;
        let mean_delay = if d.count_measurements != 0 {
            d.sum_delay_us / d.count_measurements
        } else {
            0
        };

        let hist: String = d
            .time_hist_us
            .into_iter()
            .fold(String::new(), |mut output, e| {
                let _ = write!(output, "{e:},");
                output
            });

        let bw_kbit_s =
            (p.rx_packets * u32::try_from(packet_len).unwrap() * 8) / ((d.sum_delay_us / 1000) + 1);

        println!(
            "{:5}*{:4}Pkt*{:7}, {:4}.{:01},{:4}.{:01},{:4}.{:01},{:6},{:6},{:4}.{:01},{:6},  {}",
            packet_len,               // packet len
            burst_size,               // burst size
            nbr_packets / burst_size, // nbr bursts
            mean_delay / 1000,
            (mean_delay / 100) % 10, // mean delay
            d.min_delay_us / 1000,
            (d.min_delay_us / 100) % 10, // min delay
            d.max_delay_us / 1000,
            (d.max_delay_us / 100) % 10, // max delay
            p.rx_seq_error
                + p.tx_error
                + p.rx_error
                + p.rx_timeout
                + p.rx_len_error
                + p.rx_len_error, // all local errors
            p.rx_remote_error,           // remote errors
            bw_kbit_s / 1000,
            (bw_kbit_s / 100) % 10, // bandwith in kbit/s
            (1000 * p.rx_packets) / ((d.sum_delay_us / 1000) + 1), // pps
            hist
        ); // histogram
           //println!("{:?}", self.measuements);
    }

    pub fn clear_stat(&mut self) {
        self.stats.clear();
        //println!("{:?}", self);
    }

    pub fn stat_has_erors(&self) -> bool {
        let s = &self.stats.packet_stats;
        s.tx_error != 0
            || s.rx_error != 0
            || s.rx_timeout != 0
            || s.rx_seq_error != 0
            || s.rx_len_error != 0
            || s.rx_pld_error != 0
            || s.rx_remote_error != 0
    }

    fn send_recive(
        &mut self,
        packet_len: usize,
        nbr_packets: u32,
        burst_size: u32,
        playload_type: packet::PayloadType,
    ) -> bool {
        // adjust nbr_packets to be a multiple of burst size
        let nbr_packets = if nbr_packets % burst_size == 0 {
            nbr_packets
        } else {
            ((nbr_packets / burst_size) + 1) * burst_size
        };
        let burst_counts = nbr_packets / burst_size;
        let run_start_time = Instant::now();
        let mut last_relax_index = 0;
        for burst_count in 0..burst_counts {
            let measurement_start_time = Instant::now();
            for count in 0..burst_size {
                let _ = self.send(
                    burst_count * burst_size + count + 1,
                    nbr_packets,
                    packet_len,
                    playload_type,
                );
            }
            for count in 0..burst_size {
                let _ = self.recive(
                    burst_count * burst_size + count + 1,
                    nbr_packets,
                    packet_len,
                    playload_type,
                );
            }
            let measurement_elapsed = measurement_start_time.elapsed();
            self.stats
                .delay_stats
                .add_measurement(as_u32(measurement_elapsed.as_micros()));

            // add periodic delay to limit load of system
            // add 25ms delay every 250ms
            let run_elapsed = run_start_time.elapsed().as_micros();
            let relax_index = run_elapsed / 250_000;
            if last_relax_index != relax_index {
                last_relax_index = relax_index;
                thread::sleep(Duration::from_millis(25));
            }
        }
        self.print_statistics(packet_len, nbr_packets, burst_size);
        let has_error = self.stat_has_erors();
        self.clear_stat();
        // add 25ms delay to limit load of system
        thread::sleep(Duration::from_millis(25));
        has_error
    }

    pub fn evaluate_mode(
        mode: String,
        config: &Option<std::path::PathBuf>,
    ) -> Result<Vec<[u32; 2]>, Error> {
        match mode.as_str() {
            "std" => Ok(vec![
                [200, 1],
                [400, 1],
                [800, 1],
                [1000, 1],
                [1400, 1],
                [1520, 1],
                [5000, 1],
                [200, 2],
                [400, 2],
                [800, 2],
                [1000, 2],
                [1400, 2],
                [1520, 2],
                [5000, 2],
                [200, 8],
                [400, 8],
                [800, 8],
                [1000, 8],
                [1400, 8],
                [1520, 8],
                [4500, 8],
            ]),
            "reduced" => Ok(vec![
                [200, 1],
                [400, 1],
                [800, 1],
                [1000, 1],
                [1400, 1],
                [1512, 1],
                [1514, 1],
                [200, 2],
                [400, 2],
                [800, 2],
                [1000, 2],
                [1400, 2],
                [1512, 2],
                [1514, 2],
                [200, 3],
                [400, 3],
                [800, 3],
                [1000, 3],
            ]),
            "max-pdu-size" => Ok(vec![
                [1512, 1],
                [1514, 1],
                [1516, 1],
                [1518, 1],
                [1520, 1],
                [1522, 1],
            ]),
            _ => match config {
                Some(path_buf) => {
                    let settings = Settings::try_new_file(path_buf)?;
                    for pattern in settings.pattern {
                        if pattern.1.name == mode {
                            return Ok(pattern.1.sequence);
                        }
                    }
                    Err(Error::CannotFindPattern(mode))
                }
                None => Err(Error::MissingConfigFile()),
            },
        }
    }

    pub fn try_run_perf(
        mode: String,
        ip_addr: IpAddr,
        port: u16,
        nbr_packets: u32,
        nbr_runs: u32,
        playload_type: packet::PayloadType,
        config: &Option<std::path::PathBuf>,
        rx_timeout: std::time::Duration,
    ) -> Result<bool, Error> {
        let mut client = Client::try_new(ip_addr, port, rx_timeout)?;
        let mut has_errors = false;
        let vec = Client::evaluate_mode(mode, config);

        match vec {
            Ok(vec) => {
                for run in 1..=nbr_runs {
                    println!("\n------ udp performance client run {run} of {nbr_runs} ------");
                    Self::print_header();
                    for p in &vec {
                        has_errors |=
                            client.send_recive(p[0] as usize, nbr_packets, p[1], playload_type);
                    }
                }
                if has_errors {
                    println!("\n****** UDP CLIENT FINISHED WITH ERROR ******\n");
                } else {
                    println!("\n****** udp client finished sucessfully ******\n");
                }
                Ok(has_errors)
            }
            Err(err) => {
                println!("\n****** UDP CLIENT FAILED: '{err}' ******\n ");
                Err(err)
            }
        }
    }
}
