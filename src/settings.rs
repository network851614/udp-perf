use crate::client;
use serde::Deserialize;
use std::collections::HashMap;

#[derive(Debug, Deserialize, PartialEq, Eq, Clone)]
pub struct Pattern {
    pub name: String,
    pub description: String,
    pub sequence: Vec<[u32; 2]>,
}

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
/// Pattern
pub struct Settings {
    pub description: String,
    pub pattern: HashMap<String, Pattern>,
}

impl Settings {
    /// create settings from toml configfile
    ///
    /// # Errors
    /// - when problems reading or parsing the configfile occur
    pub fn try_new_file(filepath: &std::path::PathBuf) -> Result<Self, client::Error> {
        let toml = std::fs::read(filepath)
            .map_err(|e| client::Error::CannotReadConfigFile(e, filepath.into()))?;
        let toml_str = std::str::from_utf8(&toml).map_err(client::Error::CannotDecodeConfigFile)?;
        let settings = toml::from_str(toml_str).map_err(client::Error::CannotParseConfigFile)?;
        Ok(settings)
    }

    /// create settings from toml configstring
    ///
    /// # Errors
    /// - when problems parsing the configstring occur
    #[allow(unused)]
    pub fn try_new_toml_str(content: &str) -> Result<Self, client::Error> {
        let settings = toml::from_str(content).map_err(client::Error::CannotParseConfigFile)?;
        Ok(settings)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_configfile_ok() {
        const CONFIG: &str = "
        description = 'This is my collection of patterns'

        [pattern.simple]
        name = 'simple'
        description = 'simple pattern for low end equipement'
        sequence = [[1000, 2],
                    [3000, 3]]

        [pattern.complex]
        name = 'complex'
        description = 'complex pattern for high end equipement'
        sequence = [[1000, 2],
                    [3000, 3],
                    [5000, 8]]
        ";
        let settings = Settings::try_new_toml_str(CONFIG).expect("config error");
        println!("settings = {settings:?}");
        assert_eq!("This is my collection of patterns", settings.description);
        let pattern_keys = settings.pattern.keys();
        assert_eq!(2, pattern_keys.len());

        let pattern = settings.pattern.get("complex").unwrap();
        assert_eq!("complex".to_string(), pattern.name);
        assert_eq!(
            "complex pattern for high end equipement".to_string(),
            pattern.description
        );
        assert_eq!(vec![[1000, 2], [3000, 3], [5000, 8]], pattern.sequence);

        let pattern = settings.pattern.get("simple").unwrap();
        assert_eq!("simple".to_string(), pattern.name);
        assert_eq!(
            "simple pattern for low end equipement".to_string(),
            pattern.description
        );
        assert_eq!(vec![[1000, 2], [3000, 3]], pattern.sequence);
    }

    #[test]
    fn test_configfile_malformat() {
        const CONFIG: &str = "
        description = 'This is my collection of patterns'

        [pattern.simple]
        name = 'simple'
        description = 'simple pattern for low end equipement'
        sequence = [[1000, 2],
                    [3000, 3]
        ";
        assert!(matches!(
            Settings::try_new_toml_str(CONFIG).err().unwrap(),
            client::Error::CannotParseConfigFile(_)
        ));
    }
}
