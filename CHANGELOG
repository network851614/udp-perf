# Change Log
All notable changes to this project will be documented in this file.


## [1.2.0] - 2025-02-05

### Added
- Added parameter for rx timeout in ms

### Fixed
- test for settings

## [1.1.1] - 2024-11-11

### Fixed
- fixed help

## [1.1.0] - 2024-10-15

### Added
- Added configfile to specify own packet sequences

### Fixed
- return program with error if a tests were not successful
- delay of first replied packet when server started (approx 35ms)
  replaced crate sscanf crate scanf
  sscanf uses regexes which can introduce random delay see
  https://docs.rs/regex/latest/regex/#avoid-re-compiling-regexes-especially-in-a-loop
- reduced use of crates and its features
- fix all clippy warnings

## [1.0.1] - 2024-09-25

### Fixed
- client: make sure number of packets is a multiple of burst_size
  This avoid empty sequences to be sent

- client: avoid division by zero

- zephyr: add cpp and full libcpp to dependencies

## [1.0.0] - 2024-09-23

### Added
- Added support for zephyr see zephyr/README.md

### Changed
- Breaking Change: removed unused param s= and d= from header
  this requires update from server and client

- Breaking Change: added param t= to the header to specify payload-type
  the implementation is not fully complete but it can be added without
  breaking the header again.
  Today the alternate-incrementing pattern is implemented and an option
  to skip the comparison on the server-side
  this change requires update from server and client

### Fixed
- fix param o= in header nbr of packets match also with bursts
  if the nbr of packets modulo burst size is not 0
 
## [0.3.0] - 2024-07-25

### Fixed
- fix packet size of ipv6 packets,
  reduce size of payload according ipv4/ipv6 header size to not exceed specified size of the line

## [0.2.0] - 2024-04-05

### Fixed
- fix server for devices with IPv4 only, try to bind with IPv6 and fallback to IPv4

## [0.1.0] - 2024-04-04
 
### Added
- add the first draft version of the tool written in RUST.
  there are a lot of ideas to improve the tool to make it more versatile
- add support for IPv6

### Changed
- various logging
 
### Fixed
- fix recive timeout 
