#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utility>

#include <errno.h>
#include <unistd.h>
#include <zephyr/net/net_ip.h>
#include <zephyr/net/socket.h>
#include <zephyr/sys_clock.h>

#include <zephyr/logging/log.h>
#include <zephyr/shell/shell.h>

#include "UdpPerf.h"

// Loglevel of main function
LOG_MODULE_REGISTER(udpperf, LOG_LEVEL_INF);

static void udpPerfThread(void* t1, void* t2, void* t3);
K_THREAD_DEFINE(udpPerfTid, CONFIG_UDPPERF_STACK_SIZE, udpPerfThread, NULL, NULL, NULL, CONFIG_UDPPERF_THREAD_PRIORITY, 0, 2000);

#define ETH_HDR_LEN 14
#define HDR_LEN_IPV4 20
#define HDR_LEN_IPV6 40
#define UDP_HDR_LEN 8
#define PKT_HDR_LEN_IPV4 (ETH_HDR_LEN + HDR_LEN_IPV4 + UDP_HDR_LEN)
#define PKT_HDR_LEN_IPV6 (ETH_HDR_LEN + HDR_LEN_IPV6 + UDP_HDR_LEN)

// smaller operatur for type NetAddr
bool operator<(const UdpPerf::NetAddr& lhs, const UdpPerf::NetAddr& rhs)
{
  // not same ip version
  if (lhs.family < rhs.family) {
    return true;
  } else {
    // same ip version
    if (lhs.family == AF_INET6) {
      return (memcmp(&lhs.in6_addr, &rhs.in6_addr, sizeof(rhs.in6_addr)) < 0);
    } else {
      return (memcmp(&lhs.in_addr, &rhs.in_addr, sizeof(rhs.in_addr)) < 0);
    }
  }
  return false;
}

/**
 * implentation of UdpPerf
 */
UdpPerf::UdpPerf()
    : server()
{
}

/**
 * implentation of server
 */
UdpPerf::Server::Server()
{
  int retval = 0;
  memset(pollFds, 0, sizeof(pollFds));
  for (auto type = 0; type < SOCKET_TYPE_COUNT; type++) {
    pollFds[type].fd = -1;
  }

#if defined(CONFIG_NET_IPV4)
  int fd = zsock_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (fd >= 0) {
    struct sockaddr_in server_addr = {
      .sin_family = AF_INET,
      .sin_port = htons(CONFIG_UDPPERF_PORT),
      .sin_addr = INADDR_ANY_INIT,
    };
    retval = zsock_bind(fd, (struct sockaddr*)&server_addr, sizeof(server_addr));
    if (0 != retval) {
      LOG_ERR("Server: cannot bind ipv4 port");
      return;
    }
    pollFds[SOCKET_TYPE_IPV4] = zsock_pollfd { .fd = fd, .events = ZSOCK_POLLIN, .revents = 0 };
  } else {
    LOG_ERR("Server: cannot create ipv4 socket %d", fd);
    // return;
  }
#endif

#if defined(CONFIG_NET_IPV6)
  int fd6 = zsock_socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
  if (fd6 >= 0) {
    int socketOpt = 0;
    socklen_t socketOptLen = sizeof(socketOpt);
    retval = zsock_setsockopt(fd6, IPPROTO_IPV6, IPV6_V6ONLY, &socketOpt, socketOptLen);
    if (retval >= 0) {
      struct sockaddr_in6 server_addr = {
        .sin6_family = AF_INET6,
        .sin6_port = htons(CONFIG_UDPPERF_PORT),
        .sin6_addr = IN6ADDR_ANY_INIT,
      };
      retval = zsock_bind(fd6, (struct sockaddr*)&server_addr, sizeof(server_addr));
      if (0 != retval) {
        LOG_ERR("Server: cannot bind ipv6 port");
        return;
      }
      pollFds[SOCKET_TYPE_IPV6] = zsock_pollfd { .fd = fd6, .events = ZSOCK_POLLIN, .revents = 0 };

    } else {
      LOG_ERR("Server: cannot set sockopt IPV6_V6ONLY");
      return;
    }
  } else {
    LOG_ERR("Server: cannot create ipv6 socket %d", fd6);
    return;
  }
#endif
}

int UdpPerf::Server::run()
{
  for (auto type = 0; type < SOCKET_TYPE_COUNT; type++) {
    if (pollFds[type].fd < 0) {
      LOG_ERR("Server: not all sockets are created");
      return -1;
    }
  }

  int retval = 0;
  while (true) {
    retval = zsock_poll(pollFds, sizeof(pollFds) / sizeof(struct zsock_pollfd), 10000);
    if (retval < 0) {
      LOG_WRN("Server: zsock_poll returned error %d", retval);
    } else if (retval == 0) {
      // LOG_INF("Server: zsock_poll timeout");
    } else {
      struct sockaddr clientSocketAddr;
      memset(&clientSocketAddr, 0, sizeof(clientSocketAddr));
      NetAddr clientAddr;
      memset(&clientAddr, 0, sizeof(clientAddr));

#if defined(CONFIG_NET_IPV4)
      if (pollFds[SOCKET_TYPE_IPV4].revents) {
        socklen_t alen = sizeof(struct sockaddr_in);
        retval = zsock_recvfrom(pollFds[SOCKET_TYPE_IPV4].fd, buf, CONFIG_UDPPERF_MAX_PACKET_SIZE, 0, &clientSocketAddr, &alen);
        clientAddr.family = net_sin(&clientSocketAddr)->sin_family;
        clientAddr.in_addr = net_sin(&clientSocketAddr)->sin_addr;
        handlePacket(retval, clientAddr, clientSocketAddr);
      }
#endif
#if defined(CONFIG_NET_IPV6)
      if (pollFds[SOCKET_TYPE_IPV6].revents) {
        socklen_t alen = sizeof(struct sockaddr_in6);
        retval = zsock_recvfrom(pollFds[SOCKET_TYPE_IPV6].fd, buf, CONFIG_UDPPERF_MAX_PACKET_SIZE, 0, &clientSocketAddr, &alen);
        clientAddr.family = net_sin6(&clientSocketAddr)->sin6_family;
        clientAddr.in6_addr = net_sin6(&clientSocketAddr)->sin6_addr;
        handlePacket(retval, clientAddr, clientSocketAddr);
      }
#endif
    }
  }
  return 0;
}

void UdpPerf::Server::handlePacket(int rxLen, NetAddr& clientAddr, struct sockaddr& clientSocketAddr)
{
  unsigned int rxError = 0; // currently we have no global errors to report

  if (0 >= rxLen) {
    LOG_ERR("Server: Unexpected receive %d (%d)", rxLen, errno);
    return;
  }

  unsigned int rxLenWithHeader = rxLen + ((clientAddr.family == AF_INET6) ? PKT_HDR_LEN_IPV6 : PKT_HDR_LEN_IPV4);

  // check if client is already registered if not add
  ClientData clientData;
  auto client = clientDataMap.try_emplace(clientAddr, clientData);

  if (client.second) {
    char hr_addr[NET_IPV6_ADDR_LEN];
    if (client.first->first.family == AF_INET6) {
      LOG_INF("Server: register addr %s for client",
          net_addr_ntop(client.first->first.family, &client.first->first.in6_addr, hr_addr, NET_IPV6_ADDR_LEN));
    } else if (client.first->first.family == AF_INET) {
      LOG_INF("Server: register addr %s for client",
          net_addr_ntop(client.first->first.family, &client.first->first.in_addr, hr_addr, NET_IPV4_ADDR_LEN));
    }
    client.first->second.expSeq = 1;
    strcpy(client.first->second.name, hr_addr);
  }
  ClientData* clientHdl = &client.first->second;

  RxPktInfo rxPktInfo;
  sscanf(buf, "UPi q=%u o=%u z=%u t=%u\n", &rxPktInfo.rxSeq, &rxPktInfo.rxOf, &rxPktInfo.rxLen, &rxPktInfo.pldType);

  if (rxLenWithHeader == rxPktInfo.rxLen) {
    if (rxPktInfo.rxSeq == 1) {
      // first packet of seq clear the stats
      clientHdl->stats.clean();
      clientHdl->expSeq = 1;
      LOG_DBG("Server: Rx-Msg %10u of %10u from %s size %5u(test started)", rxPktInfo.rxSeq, rxPktInfo.rxOf, clientHdl->name,
          rxPktInfo.rxLen);
    }
    if (rxPktInfo.rxSeq == clientHdl->expSeq) {
      clientHdl->expSeq++;
      clientHdl->stats.rxTotal++;
      checkPayload(rxLen, clientHdl, rxPktInfo);

      if (((rxPktInfo.rxSeq % 10000) == 0) || (rxPktInfo.rxSeq == rxPktInfo.rxOf)) {
        LOG_DBG("Server: Rx-Msg %10u of %10u from %s size %5u"
                "(client-err: seq=%u tx=%u len=%u) (glob-err: rx=%u) bit error=%u)",
            rxPktInfo.rxSeq, rxPktInfo.rxOf, clientHdl->name, rxPktInfo.rxLen, clientHdl->stats.seqError,
            clientHdl->stats.rxTotal, clientHdl->stats.lenError, rxError, clientHdl->stats.pldError);
      }

      if (rxPktInfo.rxSeq == rxPktInfo.rxOf) {
        /* send back remote with the last packet */
        sprintf(buf, "UPi q=%u o=%u z=%u t=%u e=%u\n", rxPktInfo.rxSeq, rxPktInfo.rxOf, rxPktInfo.rxLen, rxPktInfo.pldType,
            clientHdl->stats.seqError + clientHdl->stats.txError + clientHdl->stats.lenError + rxError + clientHdl->stats.pldError);

        clientHdl->expSeq = 1;
        clientHdl->stats.clean();
      }
#if defined(CONFIG_NET_IPV4)
#if defined(CONFIG_NET_IPV6)
      int fd = (clientAddr.family == AF_INET6) ? pollFds[SOCKET_TYPE_IPV6].fd : pollFds[SOCKET_TYPE_IPV4].fd;
#else
      int fd = pollFds[SOCKET_TYPE_IPV4].fd;
#endif
#else
      int fd = pollFds[SOCKET_TYPE_IPV6].fd;
#endif
      if (0 > zsock_sendto(fd,
              buf,
              rxPktInfo.rxLen - ((clientAddr.family == AF_INET6) ? PKT_HDR_LEN_IPV6 : PKT_HDR_LEN_IPV4),
              0,
              &clientSocketAddr,
              sizeof(clientSocketAddr))) {
        LOG_ERR("Server: Failed to send");
      }
    } else {
      LOG_ERR("Server: Wrong seq nbr %10u instead of %10u from %s size %5u (adjust seq) (client-err: seq=%u tx=%u len=%u)"
              " (glob-err: rx=%u)",
          rxPktInfo.rxSeq, clientHdl->expSeq, clientHdl->name, rxPktInfo.rxLen, clientHdl->stats.seqError,
          clientHdl->stats.txError, clientHdl->stats.lenError, rxError);

      clientHdl->expSeq = rxPktInfo.rxSeq + 1;
      clientHdl->stats.seqError++;
    }
  } else {
    clientHdl->stats.lenError++;
    LOG_ERR("Server: Invalid message size received - Expected %u Arrived %u", rxPktInfo.rxLen, rxLenWithHeader);
  }
}

void UdpPerf::Server::checkPayload(int rxLen, ClientData* clientHdl, RxPktInfo& rxPktInfo)
{
  bool printedOnce = false;
  int headerSize = strnlen(buf, sizeof(buf));

  if (rxLen > headerSize) {
    if (rxPktInfo.pldType == UNSPECIFIED) {
      // no check
      return;
    } else if (rxPktInfo.pldType == ALTERERNATING_INCREMENT) {
      for (int t = headerSize + 1; t < rxLen; t++) {
        if (buf[t] != (char)((t % 2) ? t / 2 : ~(t / 2))) {
          clientHdl->stats.pldError++;
          if (!printedOnce) {
            printedOnce = true;
            LOG_ERR("Server: Rx-Msg %10u of %10u from %s size %5u (bit error %u %02x/%02x)", rxPktInfo.rxSeq, rxPktInfo.rxOf,
                clientHdl->name, rxPktInfo.rxLen, t, buf[t], (char)t);
            LOG_HEXDUMP_DBG(buf, (uint32_t)rxLen, "buf");
          }
        }
      }
    } else {
      clientHdl->stats.pldError++;
      LOG_ERR("Server: Rx-Msg %10u of %10u from %s (unknown payload type %d)", rxPktInfo.rxSeq, rxPktInfo.rxOf, clientHdl->name,
          rxPktInfo.pldType);
    }
  } else {
    LOG_ERR("Server: Rx-Msg %10u of %10u from %s size %5u cannot do payload check %5u", rxPktInfo.rxSeq, rxPktInfo.rxOf,
        clientHdl->name, rxPktInfo.rxLen, headerSize);
  }
}

UdpPerf::Server::~Server()
{
  for (auto type = 0; type < SOCKET_TYPE_COUNT; type++) {
    if (pollFds[type].fd >= 0) {
      close(pollFds[type].fd);
    }
  }
}

int UdpPerf::run()
{
  return server.run();
}

static void udpPerfThread(void* t1, void* t2, void* t3)
{
  LOG_INF("create UdpPerf");
  UdpPerf updPerf;
  updPerf.run();
}
