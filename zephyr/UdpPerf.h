#pragma once

#include <map>
#include <vector>

class UdpPerf {
public:
  UdpPerf();
  int run();

public:
  /**
   * Statistics
   */
  class Stats {
  public:
    Stats()
        : rxTotal(0)
        , txError(0)
        , seqError(0)
        , lenError(0)
        , reXmit(0)
        , pldError(0)
    {
    }

    void clean()
    {
      rxTotal = 0;
      txError = 0;
      seqError = 0;
      lenError = 0;
      reXmit = 0;
      pldError = 0;
    }

  public:
    unsigned int rxTotal; /* total rx packets */
    unsigned int txError; /* total tx errors */
    unsigned int seqError; /* total tx errors */
    unsigned int lenError; /* total len errors */
    unsigned int reXmit; /* pkts re transmitted */
    unsigned int pldError; /* total bit errors in payload */
  };

  /**
   * Client Data
   */
  class ClientData {
  public:
    ClientData()
        : expSeq(1)
        , stats()
    {
      memset(name, 0, sizeof(name));
    }

  public:
    unsigned int expSeq; /* next expected seq */
    Stats stats; /* statistics */
    char name[NET_IPV6_ADDR_LEN]; /* name of client / ip addr */
  };

  enum PayloadType {
    UNSPECIFIED = 0,
    ALTERERNATING_INCREMENT = 1,
  };

  /**
   * extracted data of the recived packet
   */
  class RxPktInfo {
  public:
    RxPktInfo()
        : rxSeq(0)
        , rxOf(0)
        , rxLen(0)
        , pldType(UNSPECIFIED)
    {
    }
    unsigned int rxSeq; // rx sequence number
    unsigned int rxOf; // rx sequence len
    unsigned int rxLen; // rx packet len
    unsigned int pldType; // rx payload type
  };

  typedef struct net_addr NetAddr;
  typedef std::map<NetAddr, ClientData> ClientDataMap;

  /**
   * Server
   */
  class Server {
  public:
    Server();
    ~Server();
    int run();
    void handlePacket(int rxLen, NetAddr& clientAddr, struct sockaddr& clientSocketAddr);
    void checkPayload(int rxLen, ClientData* clientHdl, RxPktInfo& rxPktInfo);

    typedef enum {
#if defined(CONFIG_NET_IPV4)
      SOCKET_TYPE_IPV4,
#endif
#if defined(CONFIG_NET_IPV6)
      SOCKET_TYPE_IPV6,
#endif
      SOCKET_TYPE_COUNT,
    } SocketType;

  protected:
    struct zsock_pollfd pollFds[SOCKET_TYPE_COUNT]; // on fd for each IP-SocketType
    ClientDataMap clientDataMap;
    char buf[CONFIG_UDPPERF_MAX_PACKET_SIZE]; // only single threaded use
  };

protected:
  Server server;
};
