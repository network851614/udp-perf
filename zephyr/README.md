# udp-perf implementation for zephyr

## Limitations
The Zephyr implements a server only.   
Due to missing udp-fragmentation support in the networkstack the max packet size is limited to the max. Ethernet packet size.    
Also the allowed burstiness of the packets is given from the hw and the network configuration.  
use the mode  
```
--mode reduced
```

## Integration in Zephyr SW
The integration is tested with Zephyr 3.7

Add the config in your project file, include the CMakeLists.txt
```
  CONFIG_UDPPERF=y
  CONFIG_NET_UDP=y
  CONFIG_CPP=y
  # c++17 or later
  CONFIG_STD_CPP17=y
  CONFIG_REQUIRES_FULL_LIBCPP=y
```  
a own thread will be spawned

Support for IPv4
```
  CONFIG_NET_IPV4=y
```  

Support for IPv6
```
  CONFIG_NET_IPV6=y
  CONFIG_NET_IPV6_DAD=y
  CONFIG_NET_IPV4_MAPPING_TO_IPV6=n # the mapping does not work
```  

both IPv4 and IPV6 can be supported

